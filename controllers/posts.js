const { Post, User } = require('../models');

module.exports = {
    async createPost(req, res) {
        const { userUuid, body } = req.body;
        
        try {
            const user = await User.findOne({ where: { uuid: userUuid }});

            const post = await Post.create({ body, userId: user.id});

            return res.json(post);
        } catch (error) {
            console.error(error.message);
            return res.status(500).json(error);
        }
    },

    async getPosts(req, res) {
        try {
            const posts = await Post.findAll({ include: [{model: User, as: 'user'}] });

        return res.json(posts);
        
        } catch (error) {
            console.error(error.message);
            return res.status(500).json(error);
        };
    },

    async findOnePost(req, res) {
        const uuid = req.params.uuid
        try {
            const post = await Post.findOne({
                where: {uuid},
                include: 'user'
            })

            return res.json(post)
        } catch (error) {
            console.error(error.message);
            return res.status(500).json(error);
        }
    },

    async deletePosts(req, res) {
        const uuid = req.params.uuid
        try {

            await Post.destroy({
                where: {uuid}
            });

            return res.json({ message: 'Post deleted!'})
        } catch (error) {
            console.error(error.message);
            return res.status(500).json(error);
        }
    },

    async updatePosts(req, res) {
        const uuid = req.params.uuid
        const { body } = req.body
        try {
            const post = await Post.findOne({
                where: { uuid: uuid}  
            });

            post.body = body

            await post.save()

            return res.json(post)
        } catch (error) {
            console.error(error.message);
            return res.status(500).json(error);
        }
    }
}
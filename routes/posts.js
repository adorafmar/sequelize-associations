const router = require('express').Router();

const postsController = require('../controllers/posts');

router.post('/posts', postsController.createPost);

router.get('/posts', postsController.getPosts)

router.get('/posts/:uuid', postsController.getPosts)

router.delete('/posts/:uuid', postsController.deletePosts)

router.put('/posts/:uuid', postsController.updatePosts)

module.exports = router;
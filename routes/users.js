const router = require('express').Router();

const usersController = require('../controllers/users');

router.post('/users', usersController.createUser);

router.get('/users', usersController.getUsers)

router.get('/users/:uuid', usersController.findOneUser)

router.delete('/users/:uuid', usersController.deleteUser)

router.put('/users/:uuid', usersController.updateUsers)

module.exports = router


const express = require('express');
const cors = require('cors');

const { sequelize, User } = require('./models');

require('dotenv').config();

const port = process.env.PORT || 5000

const app = express();

app.use(cors());
app.use(express.json());

app.use('/api', require('./routes/users'))
app.use('/api', require('./routes/posts'))

app.listen(port, async () => {
    console.log(`Server started on port ${port}`);
    await sequelize.authenticate();
    console.log('Database connection established');
});
    
